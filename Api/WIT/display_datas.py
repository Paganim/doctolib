import yaml
import matplotlib.pyplot as plt

print("===================Entities===================\n")

with open("datas/entities.yml", 'r') as stream:
    output = yaml.load(stream, Loader=yaml.FullLoader)
    print(*output['Entities'], sep = "\n")

    print("\n\n ************* Entities for graph **************\n")
    for pos, intent in enumerate(output['Entities']):
        char = chr(ord('A') + pos)
        if pos >= 26:
            char += char
        #print(f'dot.node(\'{char}\', \'{intent}\')')
        #arra = [ '\'_\'' for i in range(25)]
        #print("E" + f'{intent:22}' + " = " + "[" + ', '.join(arra) + "]")
    #print("[E" + ', E'.join(output['Entities']) + "]")
    print("[\"E" + '\", \"E'.join(output['Entities']) + "\"]")

print("\n=================Intents=====================\n")


with open("datas/intents.yml", 'r') as stream:
    output = yaml.load(stream, Loader=yaml.FullLoader)
    intents = output['Intents']
    for pos, intent in enumerate(intents):
        print(f'{pos + 1:3d}, {intent}')

    print("\n\n ************* Intents for graph **************\n")
    for pos, intent in enumerate(intents):
        char = chr(ord('a') + pos)
        if pos >= 26:
            char += char
        #print(f'dot.node(\'{char}\', \'{intent}\')')
        #print("I" + intent + " = ")
        #arra = [ '\'_\'' for i in range(25)]
        #print("I" + f'{intent:22}' + " = " + "[" + ', '.join(arra) + "]")
    #print("[I" + ', I'.join(intents) + "]")
    print("[\"I" + '\", \"I'.join(intents) + "\"]")


print("\n===================Utterances=========================\n")

with open("datas/utterances.yml", 'r') as stream:
    output = yaml.load(stream, Loader=yaml.FullLoader)
    sentences = output['Utterances']

    hist_data = []

    for element in sentences:
        sentence = element['text']
        index = element['int']
        intent = intents[index]
        print(f'{intent:21} ==> {sentence}')

        hist_data.append(index)

    hist_data.sort()

    nb_intents = len(intents)
    plt.xticks([i for i in range(nb_intents)])
    plt.hist(hist_data, range=(0, len(intents)), bins=nb_intents)
    #plt.show()
