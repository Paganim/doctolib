import json
import difflib

import sys

if len( sys.argv ) != 2:
    print("USAGE : python utterances_parser.py filename")
    exit()

#parse utterances and save them into a file named output-[VersionNumberOfTheTrainingData].txt
def parse_utterance(filename, directory):
    with open(directory + "/" +filename) as json_file:
        utterances = []
        data = json.load(json_file)

        for utterance in data['utterances']:
            entities = []

           # add entity and avoid duplication
            [entities.append(entity['entity'].split(':')[1]) for entity in utterance['entities']
            if entity['entity'].split(':')[1] not in entities]

            # concatenate entities as 'entity,' format
            entities_str = ', '.join(entities)

            #for intents out of scope
            if "intent" not in utterance:
                continue

            line = 'intent : ' + utterance['intent'] + ' / entities : ' + entities_str

            # avoid duplication
            if line not in utterances:
                utterances.append(line)
            utterances.sort(key=lambda intent: intent.split()[2])

    output_number = filename.split('-')[1].split('.')[0]

    with open(directory + "/output-" + output_number + ".txt", 'w+') as file:
        for items in utterances:
            file.write('%s\n' % items)
        file.close()
    return int(output_number)



#parser for utterance with filename
#save it in a file
#compare with the previous file


def parse_and_compare_utterance(filename):
    directory, filename= filename.split('/')

    file_number = parse_utterance(filename, directory)

    new_file = directory + "/output-" + str(file_number) + ".txt"
    with open(new_file) as f1:
        f1_text = f1.readlines()

    if file_number == 1:
        for line in f1_text:
            print(line)
        return

    file_number_to_compare = file_number - 1

    old_file = directory + '/output-' + str(file_number_to_compare) + ".txt"


    with open(old_file) as f2:
        f2_text = f2.readlines()
    # Find and print the diff:
    text=''

    #print only differences without context with n=0
    for line in difflib.unified_diff(f1_text, f2_text, fromfile=new_file, tofile=old_file, lineterm="", n=0):
        print(line)

parse_and_compare_utterance(sys.argv[1])
