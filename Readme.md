# **Chatbot doctolib** 
## **Présentation**

Ce chatbot a été réalisé dans le cadre d'un projet EPITA.
Les étudiants qui ont participés à la création de ce bot sont : Benjamin Decreusefond, Morgan Vaterkowski, Jake Penney,  Diane Véra Ngako Tchouansi, Ines Khemir.

Il permet de répondre à une problématique:

- Permettre aux personnes dysorthographiques d'accéder à une aide en ligne pour le site doctolib via une extension Chrome

## Installation

- Etape 1: Télécharger ou cloner ce git sur votre ordinateur.

- Etape 2: Passer Google chrome en mode dévélopeur.

- Etape 3: Ouvrir les extensions chrome.

- Etape 4: Glisser déposer les fichiers de ce projet dans vos extensions et suivez la procédure.

## **Fonctionnement de l'Extension**

Cette extension permet aux utilsateurs Chrome d'avoir un outil d'aide directement accessible depuis le navigateur.

Elle a été réalisée en JS et en HTML.

## **Fonctionnement du bot**
Le bot fonctionne à l'aide de l'API Wit.ai de Facebook.

Pour délivrer une reponse, la question de l'utilisateur suit le chemin suivant :
* La question est d'abord envoyée à un premier bot qui nous permet de résumer la question en une intention utilisateur accompagnée des entités qui ont permis cette détection.
* L'intention et les entités sont ensuite récupérées afin d'être regroupé en une intention de réponse du bot
* Grâce à ces intentions de réponse nous pouvons aiguiller l'utilisateur vers la catégorie de la F.A.Q doctolib la plus pertinente.
