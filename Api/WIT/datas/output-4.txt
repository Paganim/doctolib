intent : about_doctolib / entities : doctolib, price, help
intent : about_doctolib / entities : doctolib, help
intent : about_doctolib / entities : doctolib, price, client, help
intent : about_doctolib / entities : help, video_call
intent : about_doctolib / entities : help, doctolib
intent : about_doctolib / entities : doctolib
intent : account_creation / entities : account_creation, help
intent : account_creation / entities : help, email, account, doctolib
intent : account_creation / entities : add, relative, account, doctolib
intent : account_creation / entities : account, doctolib, help, personal_info
intent : account_creation / entities : account_creation, doctolib
intent : account_creation / entities : account_creation
intent : account_creation / entities : doctolib, account_creation
intent : account_creation / entities : account, doctolib, help, email
intent : account_deletion / entities : account, doctolib, cancel
intent : account_deletion / entities : doctolib, account, cancel, help
intent : account_deletion / entities : account, cancel
intent : account_deletion / entities : doctolib, account, cancel
intent : account_relative / entities : help, relative, doctolib, add
intent : account_relative / entities : add, account, doctolib, relative
intent : account_relative / entities : relative, add
intent : account_relative / entities : help, account, add
intent : appoint_vaccination / entities : help, location, vaccination, doctor
intent : appoint_vaccination / entities : help, vaccination, doctor
intent : appoint_vaccination / entities : appointment, vaccination, location, help, urgence_did_not_find
intent : appoint_vaccination / entities : appointment, have_already_had, move, cancel, vaccination, doctor
intent : appoint_vaccination / entities : appointment, vaccination, covid, account, doctolib, help, display
intent : appoint_vaccination / entities : appointment, vaccination, covid
intent : appoint_vaccination / entities : appointment, vaccination, age_of_person, minor
intent : appoint_vaccination / entities : vaccination
intent : appoint_vaccination / entities : appointment, datetime, vaccination
intent : appoint_vaccination / entities : appointment, vaccination, move
intent : appoint_vaccination / entities : help, appointment, vaccination, second
intent : appointment / entities : greating, ask_greating, appointment, doctor
intent : appointment / entities : greating, appointment, doctor
intent : appointment / entities : greating, patient_name, age_of_person, appointment, doctor
intent : appointment / entities : appointment, help
intent : appointment / entities : help, appointment
intent : appointment / entities : modify, doctor, help
intent : appointment / entities : appointment, doctor
intent : appointment / entities : appointment, client, age_of_person, vaccination
intent : appointment / entities : greating, appointment, doctor, datetime
intent : appointment / entities : help, appointment, vaccination
intent : appointment / entities : appointment, doctor, datetime
intent : appointment / entities : help, doctor
intent : appointment / entities : help, doctor, location
intent : appointment / entities : appointment
intent : appointment / entities : appointment, datetime, doctor
intent : appointment / entities : appointment, doctor, location
intent : appointment / entities : appointment, location, doctor
intent : appointment_covid / entities : help, vaccination, location
intent : appointment_covid / entities : vaccination, datetime
intent : appointment_covid / entities : covid
intent : appointment_help / entities : help, appointment
intent : appointment_help / entities : appointment, help
intent : appointment_help / entities : appointment, help, can_t_see
intent : appointment_help / entities : appointment
intent : appointment_info / entities : doctor_info, doctor, doctor_name, help
intent : appointment_relative / entities : help, appointment, relative
intent : appointment_relative / entities : appointment, relative
intent : ask_greating / entities : greating, ask_greating
intent : ask_greating / entities : ask_greating
intent : ask_greating / entities : doctolib, help
intent : ask_greating / entities : help, doctolib
intent : ask_greating / entities : help, ask_greating
intent : ask_greating / entities : ask_greating, help
intent : asking_help / entities : help
intent : asking_help / entities : doctolib, help
intent : asking_help / entities : help, symptom
intent : cancel_appointement_vaccination_covid / entities : move, cancel, appointment, covid, help
intent : cancel_appointement_vaccination_covid / entities : cancel, appointment, vaccination
intent : cancel_appointment / entities : appointment, cancel
intent : cancel_appointment / entities : cancel, appointment, doctor
intent : contact_doctolib / entities : help, contact, doctolib
intent : contact_doctolib / entities : doctolib, contact
intent : contact_doctolib / entities : contact, doctolib
intent : covid19_informations / entities : help, vaccination, location
intent : covid19_informations / entities : vaccination, symptom
intent : covid19_informations / entities : covid, datetime, help, vaccination
intent : covid19_informations / entities : symptom, vaccination, covid, help
intent : covid19_informations / entities : vaccination, price, help, covid
intent : covid19_informations / entities : symptom
intent : covid19_informations / entities : symptom, help, covid
intent : covid19_informations / entities : symptom, datetime, covid, help
intent : covid19_informations / entities : vaccination
intent : covid19_informations / entities : symptom, covid
intent : covid19_informations / entities : help, vaccination
intent : covid19_informations / entities : symptom, covid, help
intent : covid19_informations / entities : have_already_had, covid, help, vaccination
intent : covid19_vaccination_informations / entities : help, appointment, vaccination
intent : covid19_vaccination_informations / entities : help, test_covid, vaccination
intent : covid19_vaccination_informations / entities : help, vaccination, same, doctor, location
intent : covid19_vaccination_informations / entities : vaccination, symptom
intent : covid19_vaccination_informations / entities : appointment, vaccination, confirmation
intent : covid19_vaccination_informations / entities : symptom, vaccination, covid, help
intent : covid19_vaccination_informations / entities : have_already_had, covid, period, vaccination, help
intent : covid19_vaccination_informations / entities : vaccination, covid, mandatory, help
intent : covid19_vaccination_informations / entities : vaccination, price, help
intent : covid19_vaccination_informations / entities : period, vaccination, help
intent : covid19_vaccination_informations / entities : help, vaccination, covid
intent : covid19_vaccination_informations / entities : help, vaccination, test_covid
intent : covid19_vaccination_informations / entities : help, vaccination, location, same
intent : covid19_vaccination_informations / entities : help, vaccination, second
intent : covid19_vaccination_informations / entities : have_already_had, covid, help, vaccination, mandatory
intent : covid19_vaccination_informations / entities : vaccination, mandatory, help
intent : covid19_vaccination_informations / entities : vaccination
intent : forgotten_password / entities : password, forget, help
intent : forgotten_password / entities : password, forget
intent : forgotten_username / entities : forget, username
intent : goodbye / entities : goodbye
intent : greating / entities : symptom
intent : greating / entities : greating
intent : information_refund / entities : doctor, doctor_name, help, refund
intent : insults / entities : insults
intent : log_out / entities : help, log_out, doctolib
intent : log_out / entities : log_out, help
intent : log_out / entities : log_out
intent : modify_account / entities : help, modify, account
intent : modify_account / entities : cancel, personal_info
intent : modify_account / entities : account, modify
intent : not_enough / entities : symptom
intent : not_enough / entities : help, personal_info
intent : not_enough / entities : modify, geolocalisation
intent : not_enough / entities : doctor
intent : not_enough / entities : vaccination
intent : patient_information / entities : patient_presentation, patient_name
intent : patient_information / entities : patient_presentation, patient_name, age_of_person, location
intent : patient_information / entities : age_of_person
intent : thanks / entities : goodbye, thanks
intent : thanks / entities : thanks
