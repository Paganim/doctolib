
function listenerMessage() {
    $(document).ready(function() {
        $("#sending_msg").keyup(function(event) {
            if (event.key === 'Enter') {
                $("#sending_btn").click();
            }
        });
        let button = $('#sending_btn');
        button.click(function (){onClickForm(); return false;})
    })

    return false;
}



function onClickForm(){
    //get message
    var  msg_text = $("#sending_msg").val();
	
    let msg = document.getElementById('sending_msg');

    //display message
    document.getElementById("output").innerHTML += "<div class=\"message-box-holder\"><p class=\"message-box user\">" + msg.value + "\n</p></div>";

    //display waiting dot
    var wainting_points = document.getElementById("dot-pulse-holder");
    wainting_points.style.visibility = "visible"
    //window focus down, option = delay = 0 sec
    $("html, body").animate({ scrollTop: $(document).height()-$(window).height() - 15}, 0);

    //clear textarea
    msg.value = ""

    // wait then send
    setTimeout(() => {


        //get answer
        document.getElementById("output").innerHTML += "<img class='bot-icon' src='../images/bot_icon.gif'> <div class=\"message-box-holder\"> <p class=\"message-box bot\">" + botResponse(msg_text) + "\n</p></div>";
        document.getElementById("dot-pulse-holder").style.visibility = "hidden";

        //window focus down, option = delay = 0 sec
        $("html, body").animate({ scrollTop: $(document).height()-$(window).height() }, 0);
    }, 3000);
    return false;
}

function reformatUserMsg(userMsg){
	var tmp_tab = []
	for (let i = 0; i < userMsg.length; i++){
		if (userMsg[i] == 'à'){
			tmp_tab.push('a')
		}
		else if (userMsg[i] == 'â'){
			tmp_tab.push('a')
		}
		else if (userMsg[i] == 'ä'){
			tmp_tab.push('a')
		}
		else if (userMsg[i] == 'ï'){
			tmp_tab.push('i')
		}
		else if (userMsg[i] == 'î'){
			tmp_tab.push('i')
		}
		else if (userMsg[i] == 'ô'){
			tmp_tab.push('o')
		}
		else if (userMsg[i] == 'ö'){
			tmp_tab.push('o')
		}
		else if (userMsg[i] == 'é'){
			tmp_tab.push('e')
		}
		else if (userMsg[i] == 'è'){
			tmp_tab.push('e')
		}
		else if (userMsg[i] == 'ê'){
			tmp_tab.push('e')
		}
		else if (userMsg[i] == 'ë'){
			tmp_tab.push('e')
		}
		else if (userMsg[i] == 'œ'){
			tmp_tab.push('o')
			tmp_tab.push('e')
		}
		else{
			tmp_tab.push(userMsg[i]) 
		}
	}
	return tmp_tab.join('');
}

function botResponse(userMsg) {
	userMsg = reformatUserMsg(userMsg);
    let response1 = "Failed"
    var xhttp = new XMLHttpRequest()
    xhttp.open("GET", "http://127.0.0.1:5000/?question=" + userMsg, false);
    xhttp.send();
    xhttp.onload = function () {
        if (xhttp.status != 200) { // analyze HTTP status of the response
            alert(`Error ${xhttp.status}: ${xhttp.statusText}`); // e.g. 404: Not Found
        } else { // show the result
            response1 = xhttp.responseText
        }
    };
    response1 = xhttp.response
    return response1;
}

document.addEventListener('DOMContentLoaded', function () {
    listenerMessage();
});
