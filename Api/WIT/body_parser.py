import json

import sys

if len(sys.argv) != 2:
    print("USAGE : python body_parser.py filename")
    exit()


# Parse the sentence send by to user for the test suite

def parse_body(filename):
    print(filename)
    sentences = []
    with open(filename) as json_file:
        data = json.load(json_file)
        [sentences.append(utterance['text']) for utterance in data['utterances']]

    output_number = filename.split('/')[1].split('-')[1].split('.')[0]

    directory = filename.split('/')[0]

    with open(directory + "/output-body-" + output_number + ".txt", 'w+') as file:

        for item in sentences:
            print(item)
            try:
                file.write('%s\n' % item)
            except UnicodeEncodeError:
                continue
        file.close()


parse_body(sys.argv[1])
