// sample_spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

describe('EndToEndTests', () => {
    beforeEach(() => {
        cy.visit('localhost:5001/Extension/html/popup.html')
    })

    it('Greetings test', () => {
        cy.get('.message-box.bot')
            .last()
            .contains('Bonjour, en quoi puis-je vous aider ?')
            .should('be.visible')
    })

    it('Bonjour test', () => {
        cy.get('#sending_msg')
            .should('be.visible')
            .type('Bonjour')
        cy.get('#sending_btn')
            .should('be.visible')
            .click()
        cy.get('.output')
            .last()
            .contains('En quoi puis-je vous aider ?')
            .should('be.visible')
    })

    it('Random test', () => {
        cy.get('#sending_msg')
            .should('be.visible')
            .type('klrjemltkrpege�rmgkler�')
        cy.get('#sending_btn')
            .should('be.visible')
            .click()
        cy.get('.output')
            .last()
            .contains('Je ne comprends pas')
            .should('be.visible')
    })

    it('Rdv test', () => {
        cy.get('#sending_msg')
            .should('be.visible')
            .type('Est-ce que je peux avoir un rdv')
        cy.get('#sending_btn')
            .should('be.visible')
            .click()
        cy.get('a[href*="https://www.doctolib.fr/help?origin=help-mobile#section=1"]')
            .click()
    })
})