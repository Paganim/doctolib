import os
import requests
import json
import sys
import difflib

# Send all intents and the list of entities corresponding to the training datas to the second bot
def req(path, access_token, message):
    full_url = WIT_API_HOST + path
    headers = {
        'authorization': 'Bearer ' + access_token,
        'accept': 'application/vnd.wit.' + WIT_API_VERSION + '+json'
    }
    rsp = requests.get(
        full_url + '?q=' + message,
        headers=headers,
    )
    json = rsp.json()
    return json

WIT_API_HOST = os.getenv('WIT_URL', 'https://api.wit.ai')
WIT_API_VERSION = os.getenv('WIT_API_VERSION', '20200513')
    

def send_intent_to_answer_bot(filepath):
    directory, filename= filepath.split('/')
    file_number = int(filename.split('-')[1].split('.')[0])
    old_file = directory + "/output-" + str(file_number - 1) + ".txt"

    with open(old_file) as old:

        with open(filepath) as f:
            for line in difflib.unified_diff(f.readlines(), old.readlines(), fromfile=filepath, tofile=old_file, lineterm="", n=0):
                if '@' not in line :
                    print("PROCESSING==" + line[1:])
                    returned = req('/message', 'BRZOQRV3LQ4TH47VQLOL5ZET27FKTSRU', line[1:])

print("FINISH")
send_intent_to_answer_bot(sys.argv[1])
