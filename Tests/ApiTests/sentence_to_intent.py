import subprocess
import bot as b
import time

#get input and create a file contening input and the bot answer for the test suite

subprocess.run(["touch","output.txt"])

with open("input.txt", "r") as inpu:
    with open("output.txt", "a") as output:
        for line in inpu:
            output.write(line)
            output.write(b.get_intent(line) + "\n")
            time.sleep(0.05)
