from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import sys
import json
import logging
import os
import requests
import unicodedata

from wit import Wit
from prompt_toolkit import prompt
from prompt_toolkit.history import InMemoryHistory

WIT_API_HOST = os.getenv('WIT_URL', 'https://api.wit.ai')
WIT_API_VERSION = os.getenv('WIT_API_VERSION', '20200513')


# format the request

def req(path, access_token, message):
    full_url = WIT_API_HOST + path
    headers = {
        'authorization': 'Bearer ' + access_token,
        'accept': 'application/vnd.wit.' + WIT_API_VERSION + '+json'
    }

    rsp = requests.get(
        full_url + '?q=' + message
,
        headers=headers,
    )
    json = rsp.json()
    return json

# build the request for the second bot with the user intent and a list of entities

def build_request(jsons):

    #get json
    obj = json.dumps(jsons)
    data = json.loads(obj)

    #get intents and entities
    intents = data['intents']
    entities = data['entities']


    #get the first intent
    res = "intent : " + intents[0]['name'] + " / entities : "

    #get the list of entities
    for count, entity in enumerate(entities):
        if count != 0:
            res += ", "
        res += entity.split(':')[0]
    return res

# get each bot answer

def get_intent(message):
    try:
        json_request = req('/message', 'VOFCVBCOU23YL36XPHKEP4USEUGGVQFA', message)
        msg = build_request(json_request)
        returned = req('/message', 'BRZOQRV3LQ4TH47VQLOL5ZET27FKTSRU', msg)
        intent = returned['intents'][0]['name']
    except:
        intent = "no_intent"
    return intent

# resolve intent by choosing the good answer to send to the user

def resolve_intent(intent):
    intent_resolved = True
    if intent == "about_doctolib_help":
        answer = "Si vous voulez tous savoir à propos de doctolib "
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=7&article=0\"> Cliquez ici </a>"

    elif intent == "about_doctolib_price_help":
        answer = "Pour en savoir plus à propos des offres doctolib"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=7&article=0\"> Cliquez ici </a>"

    elif intent == "account_creation_help":
        answer = "Vous voulez vous créer un compte doctolib, aucun problème ! "
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=0&article=0\"> Cliquez ici </a>"

    elif intent == "account_deletion":
        answer = "Vous voulez supprimer votre compte ? "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=0&article=5"

    elif intent == "account_modify":
        answer = "vous souhaitez modifier vos informations ? "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=0&article=4"

    elif intent == "add_relative_to_account":
        answer = "Vous voulez ajouter des informations à votre compte ? "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=0&article=4"

    elif intent == "appoint_vaccination_confirmation":
        answer = "Vous voulez confirmer votre rendez-vous vaccination ? "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=9&article=7"

    elif intent == "appoint_vaccination_covid_display_help":
        answer = "Plus d'informations ici: "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=9&article=10"

    elif intent == "appoint_vaccination_covided":
        answer = "Vous avez eu la COVID-19 il y a moins de 2 mois : vous n’avez\
        pas besoin de vous faire vacciner (vous êtes encore protégée par \
        l’immunité post-infectieuse). Vous avez eu la COVID-19 il y a plus de 2\
        mois : si vous faites partie de l’une des catégories éligibles, vous \
        pouvez vous faire vacciner. Mais vous ne recevrez qu'une seule \
        injection car vous conservez une mémoire immunitaire. Vous devez \
        néanmoins toujours prendre vos 2 rendez-vous de vaccination \
        (cliquez ici pour savoir comment faire) : votre médecin décidera lors \
        du premier rendez-vous si la seconde injection doit être maintenue \
        (si par exemple vous avez une pathologie ou un traitement qui \
        entraînent une immunodépression).\
        https://solidarites-sante.gouv.fr/grands-dossiers/vaccin-covid-19/je-suis-un-particulier/article/foire-aux-questions-la-strategie-de-vaccination"

    elif intent == "appoint_vaccination_help":
        answer = "Vous voulez vous faire vacciner ? c'est ici : "
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=9&article=0\"> Cliquez ici </a>"

    elif intent == "appoint_vaccination_move":
        answer = "Vous voulez décaler un rendez-vous ? "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=9&article=3"

    elif intent == "appoint_vaccination_moved_help":
        answer = "Votre rendez-vous à été décalé ? "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=9&article=8"

    elif intent == "appoint_vaccination_same_place_help":
        answer = "Nous vous recommandons d’effectuer vos deux injections avec le même professionnel cependant, ce n’est pas obligatoire."

    elif intent == "appoint_vaccination_urgence_help":
        answer = "Besoin d'un rendez-vous en urgence ? "
        answer += "Nous vous invitons à contacter le numéro de téléphone national :\
        0800 009 110 (disponible 7 jours sur 7 de 6h à 22h) afin de demander un\
        rendez-vous en urgence pour votre deuxième injection."

    elif intent == "appointement_help_not_doctor":
        answer = "Besoin de détails sur votre rendez-vous ? "
        answer += "https://www.doctolib.fr/help?origin=help-mobile#section=1&article=1"

    elif intent == "appointment_vaccination_minor":
        answer = "Plus d'informations sur les rendez-vous mineur ici: "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=9&article=11https://www.doctolib.fr/help?origin=help-desktop#section=9&article=11"

    elif intent == "appointment_help":
        answer = "Besoin d'aide à propos de votre rendez-vous ?"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=1\"> Cliquez ici </a>"

    elif intent == "appointment_doctor_appointInfo":
        answer = "Besoin de détails sur votre rendez-vous ?"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=1\"> Cliquez ici </a>"

    elif intent == "appointment_doctor":
        answer = "Besoin de détails sur votre rendez-vous ? "
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=1&article=1\"> Cliquez ici </a>"

    elif intent == "ask_greating_and_appointment":
        answer = "Nous allons bien, merci!\n"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=1&article=1\"> Cliquez ici </a>"

    elif intent == "appointment_doctor_patientInfo":
        answer = "Besoin de détails sur votre rendez-vous ? "
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=1&article=1\"> Cliquez ici </a>"

    elif intent == "appointment_vaccination_mandatory":
        answer = "Non le vaccin n'est pas obligatoire à l'heure actuelle et la preuve de vaccination ne pourra donc pas être exigée. De plus, le consentement de la personne à la vaccination devra être recueilli au préalable. Plus d’informations et source sur le site du gouvernement.\n"
        answer += "Pour plus d'informations :\n"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=9&article=0\"> Cliquez ici </a>"

    elif intent == "asking_help_symptom":
        answer = "Si vous souhaitez avoir plus d'informations sur vos symptomes,\
        nous vous conseillons de prendre un rendez-vous via notre plateforme :"
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=1&article=1"

    elif intent == "cancel_appointment_vaccination":
        answer = "Vous voulez annuler votre rendez-vous ? "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=9&article=4"

    elif intent == "ask_greating":
        answer = "Nous allons bien, merci !\n"
        answer += "En quoi puis-je vous aider ?"

    elif intent == "ask_greating_and_appointment":
        answer = "Nous allons bien, merci !\n"
        answer += "Besoin d'aide à propos de votre rendez-vous ?"
        answer += "https://www.doctolib.fr/help?origin=help-mobile#section=1"

    elif intent == "cancel_appointment_help":
        answer = "Toutes les informations pour annuler un rendez-vous ce trouve ici\n"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=2&article=3\"> Cliquez ici </a>"

    elif intent == "cancel_appointment":
        answer = "Toutes les informations pour annuler un rendez-vous ce trouve ici\n"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=2&article=3\"> Cliquez ici </a>"

    elif intent == "contact_doctolib":
        answer = "Merci de vous rendre sur le site web de doctolib\n"

    elif intent == "side_effect_vaccination_help":
        answer = "En cas d’urgence contactez le 15 (Samu). Sinon, contactez votre médecin traitant et suivez le guide du gouvernement pour déclarer les effets secondaires."

    elif intent == "forgotten_password":
        answer = "Vous avez oublié votre mot de passe, pas de panique on est la pour vous !\n"
        answer += "Voici un lien qui pourra vous aider\n"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-desktop#section=0&article=3&content\"> Cliquez ici </a>"

    elif intent == "forgotten_username":
        answer = "Vous avez oublié votre identifiant, pas de panique on est la pour vous !\n"
        answer += "Voici un lien qui pourra vous aider :\n"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-desktop#section=0&article=3&content\"> Cliquez ici </a>"

    elif intent == "goodbye":
        answer = "Au revoir, à bientôt !"

    elif intent == "greating":
        answer = "En quoi puis-je vous aider ?"

    elif intent == "insult":
        answer = "Ce n'est pas très gentil !"

    elif intent == "log_out_help":
        answer = "Vous souhaitez vous déconnecter\n"
        answer += "<a href=\"https://www.doctolib.fr/help?origin=help-mobile#section=0&article=2&content\"> Cliquez ici </a>"

    elif intent == "not_enough":
        answer = "Pouvez-vous reformuler votre question ?"

    elif intent == "patientInfo":
        answer = "Nous ne pouvons pas recueillir vos données personnelles. Avez-vous d'autres questions ?"

    elif intent == "refund_appointment":
        answer = "Besoin de vous faire rembourser un rendez-vous ? "
        answer += "https://www.doctolib.fr/help?origin=help-mobile#section=1&article=0&content"

    elif intent == "test_before_vaccination":
        answer = "Besoin de vous faire tester avant d'aller vous faire vacciner ? "
        answer += "https://www.doctolib.fr/help?origin=help-mobile#section=9&article=0"

    elif intent == "about_video_call":
        answer = "Plus d'information sur la vidéo-consultation ici: "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=3"

    elif intent == "appoint_relative":
        answer = "Pour rendre rendez-vous pour l'un de vos proches: "
        answer += "https://www.doctolib.fr/help?origin=help-desktop#section=5"

    elif intent == "impossible_access_appointment":
        answer = "Vous ne pouvez pas vous rendre à votre rendez-vous ? "
        answer += "https://doctolib.fr/help/#section=2&article=1&content"

    elif intent == "test_covid_symptom":
        answer = "Prenez soin de vous. Si vous présentez des symptômes propres à \
        la Covid-19, le Ministère des Solidarités et de la Santé recommande de\
        prendre immédiatement rendez-vous avec un médecin (consultation vidéo\
        ou consultation physique) et d’éventuellement passer un test de dépistage.\
        Plus d’informations : https://www.gouvernement.fr/info-coronavirus .\
        Pour rappel, les symptômes de la Covid-19 incluent des difficultés\
        respiratoires, de la toux, la présence de fièvre, des maux de tête ou\
        de gorge, des courbatures ou encore la perte de goût ou d’odorat."

    elif intent == "thanks":
        answer = "De rien !"
    else :
        intent_resolved = False
        answer = "Je ne comprends pas. <br>Pouvez-vous reformuler votre question ?"

    if intent_resolved :
        answer+= "<br><br> Avez-vous d'autres questions ?"


    return answer


# get the answer to send (it is usefull for the test suite)
def send_request(message):
    intent = get_intent(message)
    return resolve_intent(intent)
