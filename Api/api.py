import flask
import WIT.bot as bot
from flask_cors import CORS


app = flask.Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config["DEBUG"] = False


@app.route('/', methods=['GET'])
def home():
    query_parameters = flask.request.args
    question = query_parameters.get('question')
    return bot.send_request(question)

app.run()
