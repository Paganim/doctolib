import json
import difflib
import sys

if len( sys.argv ) != 2:
    print("USAGE : python utterances_parser.py filename")
    exit()

#parse utterances and save them into a file named output-[VersionNumberOfTheTrainingData].txt

def parse_utterance(filename, directory):
    with open(directory + "/" +filename) as json_file:
        output = []
        data = json.load(json_file)

        for utterance in data['utterances']:
            entities = []

            line = utterance['intent']

            #for intents out of scope
            if line in output:
                continue

            print(line)
            output.append(line)

            output.sort()

    output_number = filename.split('-')[1].split('.')[0]

    with open(directory + "/output-" + output_number + ".txt", 'w+') as file:
        for items in output:
            file.write('%s\n' % items)
        file.close()
    return int(output_number)



#parser for utterance with filename
#save it in a file
#compare with the previous file


def parse_and_compare_utterance(filename):
    directory, filename= filename.split('/')

    file_number = parse_utterance(filename, directory)

    new_file = directory + "/output-" + str(file_number) + ".txt"
    with open(new_file) as f1:
        f1_text = f1.readlines()

    if file_number == 1:
        #iterable  = iter(f1_text)
        for line, answer in zip(f1_text[0::2], f1_text[1::2]):
            print(line)
        return

    file_number_to_compare = file_number - 1

    old_file = directory + '/output-' + str(file_number_to_compare) + ".txt"

    with open(old_file) as f2:
        f2_text = f2.readlines()
    # Find and print the diff:

    #print only differences without context with n=0
    for count, line in enumerate(difflib.unified_diff(f1_text, f2_text, fromfile=new_file, tofile=old_file, lineterm="", n=0)):
        print(line)

parse_and_compare_utterance(sys.argv[1])
