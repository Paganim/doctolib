How to launch Tests

!!! Make sure you have the API running !!!

1/ Api tests
open a terminal and type "pytest"

2/ EndToEnd tests
open the "EndToEndTests" folder and open a terminal and type "sh launch_endtoendtests.sh" and follow the instructions
